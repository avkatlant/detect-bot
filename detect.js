function traceClick() {
  //Функция-обёртка для всего

  var minInterval = 100; //Интервал, через который реагируем на мышь, мс
  var timerON = false; //Состояние таймера
  var start, last; //Начало отсчёта и последняя отметка
  var mousePosition, oldMousePosition;

  function makeTime(s) {
    //Получить строку времени
    var m = s.getMinutes();
    if (m < 10) m = "0" + m;
    var c = s.getSeconds();
    if (c < 10) c = "0" + c;
    return s.getHours() + ":" + m + ":" + c;
  }

  function clickEvent() {
    //Обработчик кликов
    if (
      mousePosition.x != oldMousePosition.x &&
      mousePosition.y != oldMousePosition.y
    ) {
      oldMousePosition.x = mousePosition.x;
      oldMousePosition.y = mousePosition.y;
      document.getElementById("informationId").innerHTML =
        "Clicked at " + mousePosition.x + ", " + mousePosition.y;
      last = new Date().getTime();
    }
  }

  function mouseUpdate(event) {
    //Обработчик позиции мыши
    var now = new Date().getTime();
    if (timerON) {
      if (now - last < minInterval) {
        return;
      }
      last = now;
      mousePosition = { x: event.pageX, y: event.pageY };
      document.getElementById("informationId").innerHTML =
        "Move at " + mousePosition.x + ", " + mousePosition.y;

      document.getElementById("current-cursor").style.left =
        mousePosition.x + "px";
      document.getElementById("current-cursor").style.top =
        mousePosition.y + "px";
    } else {
      //Если нужно, делаем что-то, пока время с последней отметки не прошло
    }
  }

  document.addEventListener("DOMContentLoaded", function () {
    //Ставим обработчики по загрузке DOM
    start = new Date().getTime();
    last = new Date().getTime();
    timerON = true;
    oldMousePosition = { x: 0, y: 0 };
    document.addEventListener("click", clickEvent);
    document.addEventListener("mousemove", mouseUpdate);
    // var timer = setInterval(function () {
    //   document.getElementById("informationId").innerHTML = makeTime(new Date());
    // }, 1000); //Таймер через секунду
  });

  // document.getElementById("detect-webdriver").innerHTML =
  //   "navigator: <br />" + navigator.languages;

  var text = "";
  for (item in navigator) {
    text = text + item + ": " + JSON.stringify(navigator[item]) + "<br />";
  }
  document.getElementById("detect-webdriver").innerHTML = text;
}

traceClick();
